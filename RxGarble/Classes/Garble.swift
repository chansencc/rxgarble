//
//  Garble.swift
//  RxGarble
//
//  Created by Christian Hansen on 11/30/18.
//

import Foundation

open class Garble {
    public static let shared: Garble = Garble()
    public func garble(text: String) -> String {
        return String(text.shuffled())
    }
}
