# RxGarble

[![CI Status](https://img.shields.io/travis/Christian Hansen/RxGarble.svg?style=flat)](https://travis-ci.org/Christian Hansen/RxGarble)
[![Version](https://img.shields.io/cocoapods/v/RxGarble.svg?style=flat)](https://cocoapods.org/pods/RxGarble)
[![License](https://img.shields.io/cocoapods/l/RxGarble.svg?style=flat)](https://cocoapods.org/pods/RxGarble)
[![Platform](https://img.shields.io/cocoapods/p/RxGarble.svg?style=flat)](https://cocoapods.org/pods/RxGarble)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RxGarble is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RxGarble'
```

## Author

Christian Hansen, chansen@rhinocg.com

## License

RxGarble is available under the MIT license. See the LICENSE file for more info.
